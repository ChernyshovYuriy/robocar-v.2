#include "cmd_cntrl.h"
#include <iostream>

using namespace std;

CmdCntrl::CmdCntrl(LidarCntrl *lidar_cntrl) : lidar_cntrl_(lidar_cntrl) {
    cout << "CmdCntrl ctr" << endl;
}

CmdCntrl::~CmdCntrl() {
    cout << "CmdCntrl destr" << endl;
}

void CmdCntrl::start() {
    cout << "CmdCntrl - perform start" << endl;
    lidar_cntrl_->start();
}

void CmdCntrl::stop() {
    cout << "CmdCntrl - perform stop" << endl;
    lidar_cntrl_->stop();
}
