#include "cntrl_facade.h"
#include "../cmd/net_cmd_start.h"
#include "../cmd/net_cmd_stop.h"
#include <iostream>

using namespace std;

CntrlFacade::CntrlFacade() {
    cout << "CntrlFacade ctr" << endl;
    lidar_cntrl_ = make_unique<LidarCntrl>();
    main_motors_cntrl_ = make_unique<MainMotorsCntrl>();
    cmd_cntrl_ = make_unique<CmdCntrl>(lidar_cntrl_.get());
    cmd_map_ = make_unique<map<const char*, unique_ptr<NetCmd>>>();
    (*cmd_map_)[CMD_START] = make_unique<NetCmdStart>(cmd_cntrl_.get());
    (*cmd_map_)[CMD_STOP] = make_unique<NetCmdStop>(cmd_cntrl_.get());
    net_cntrl_ = make_unique<NetCntrl>(cmd_map_.get());
}

CntrlFacade::~CntrlFacade() {
    cout << "CntrlFacade destr" << endl;
}

void CntrlFacade::init() {
    cout << "CntrlFacade init" << endl;
    cout << "Start listen to socket ..." << endl;
    net_cntrl_->init();
}
