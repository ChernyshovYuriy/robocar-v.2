#include "lidar_cntrl.h"
#include <iostream>

using namespace std;

LidarCntrl::LidarCntrl() {
    cout << "LidarCntrl ctr" << endl;
}

LidarCntrl::~LidarCntrl() {
    cout << "LidarCntrl destr" << endl;
}

void LidarCntrl::start() {
    cout << "LidarCntrl start" << endl;
}

void LidarCntrl::stop() {
    cout << "LidarCntrl stop" << endl;
}
