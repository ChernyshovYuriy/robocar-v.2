#ifndef ROBOCAR_NET_CNTRL_H
#define ROBOCAR_NET_CNTRL_H

#include "cmd_cntrl.h"
#include "../cmd/net_cmd.h"
#include "../../net/httplib.h"
#include <map>

using namespace std;
using namespace httplib;

static const constexpr char *const CMD_START = "/start";
static const constexpr char *const CMD_STOP = "/stop";
static const constexpr char *const CMD_DISCONNECT = "/disconnect";

class NetCntrl {

public:
    explicit NetCntrl(map<const char*, unique_ptr<NetCmd>> *cmd_map);

    ~NetCntrl();

    void init();

private:
    unique_ptr<Server> svr_;
    map<const char*, unique_ptr<NetCmd>> *cmd_map_;
    atomic<bool> is_inited_{false};
};

#endif //ROBOCAR_NET_CNTRL_H
