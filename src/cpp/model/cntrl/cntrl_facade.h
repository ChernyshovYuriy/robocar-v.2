#ifndef ROBOCAR_CNTRL_FACADE_H
#define ROBOCAR_CNTRL_FACADE_H

#include "main_motors_cntrl.h"
#include "lidar_cntrl.h"
#include "net_cntrl.h"

class CntrlFacade {

public:
    CntrlFacade();

    ~CntrlFacade();

    void init();

private:
    unique_ptr<MainMotorsCntrl> main_motors_cntrl_;
    unique_ptr<LidarCntrl> lidar_cntrl_;
    unique_ptr<NetCntrl> net_cntrl_;
    unique_ptr<CmdCntrl> cmd_cntrl_;
    unique_ptr<map<const char*, unique_ptr<NetCmd>>> cmd_map_;
};

#endif //ROBOCAR_CNTRL_FACADE_H
