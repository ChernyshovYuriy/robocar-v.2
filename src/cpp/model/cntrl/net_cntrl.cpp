#include "net_cntrl.h"
#include <iostream>

static const constexpr char *const HOST = "localhost";
static const constexpr int PORT = 1234;

NetCntrl::NetCntrl(map<const char*, unique_ptr<NetCmd>> *cmd_map) : cmd_map_(cmd_map) {
    cout << "NetCntrl ctr" << endl;
    svr_ = make_unique<Server>();
}

NetCntrl::~NetCntrl() {
    cout << "NetCntrl destr" << endl;
}

void NetCntrl::init() {
    svr_->Get(CMD_START, [&](const Request &req, Response &res) {
        cout << "Received:" << CMD_START << endl;
        (*cmd_map_)[CMD_START]->execute();
    });

    svr_->Get(CMD_STOP, [&](const Request &req, Response &res) {
        cout << "Received:" << CMD_STOP << endl;
        (*cmd_map_)[CMD_STOP]->execute();
    });

    svr_->Get(CMD_DISCONNECT, [&](const Request &req, Response &res) {
        cout << "Received:" << CMD_DISCONNECT << endl;
        if (is_inited_) {
            is_inited_ = false;
            svr_->stop();
        }
    });

    is_inited_ = true;
    svr_->listen(HOST, PORT);
}
