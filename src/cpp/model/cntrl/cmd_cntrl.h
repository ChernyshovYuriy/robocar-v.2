#ifndef ROBOCAR_CMD_CNTRL_H
#define ROBOCAR_CMD_CNTRL_H

#include "lidar_cntrl.h"

class CmdCntrl {

public:
    CmdCntrl(LidarCntrl *lidar_cntrl);

    ~CmdCntrl();

    void start();

    void stop();

private:
    LidarCntrl *lidar_cntrl_;
};

#endif //ROBOCAR_CMD_CNTRL_H
