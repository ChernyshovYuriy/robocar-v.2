#ifndef ROBOCAR_LIDAR_CNTRL_H
#define ROBOCAR_LIDAR_CNTRL_H

class LidarCntrl {

public:
    LidarCntrl();

    ~LidarCntrl();

    void start();

    void stop();
};

#endif //ROBOCAR_LIDAR_CNTRL_H
