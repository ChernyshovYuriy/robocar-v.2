#ifndef ROBOCAR_NET_CMD_STOP_H
#define ROBOCAR_NET_CMD_STOP_H

#include "net_cmd.h"
#include "../cntrl/cmd_cntrl.h"

class NetCmdStop : public NetCmd {

public:
    explicit NetCmdStop(CmdCntrl *cmd_cntrl);

    ~NetCmdStop() override ;

    void execute() override ;

private:
    CmdCntrl *cmd_cntrl_;
};

#endif //ROBOCAR_NET_CMD_STOP_H
