#include "net_cmd_stop.h"
#include <iostream>

using namespace std;

NetCmdStop::NetCmdStop(CmdCntrl *cmd_cntrl) : cmd_cntrl_(cmd_cntrl) {
    cout << "NetCmdStop ctr" << endl;
}

NetCmdStop::~NetCmdStop() {
    cout << "NetCmdStop destr" << endl;
}

void NetCmdStop::execute() {
    cmd_cntrl_->stop();
}
