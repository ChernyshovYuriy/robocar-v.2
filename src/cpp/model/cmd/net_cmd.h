#ifndef ROBOCAR_NETCMD_H
#define ROBOCAR_NETCMD_H

class NetCmd {

public:
    virtual ~NetCmd() = default;

    virtual void execute() = 0;
};

#endif //ROBOCAR_NETCMD_H
