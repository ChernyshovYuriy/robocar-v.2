#include "net_cmd_start.h"
#include <iostream>

using namespace std;

NetCmdStart::NetCmdStart(CmdCntrl *cmd_cntrl) : cmd_cntrl_(cmd_cntrl) {
    cout << "NetCmdStart ctr" << endl;
}

NetCmdStart::~NetCmdStart() {
    cout << "NetCmdStart destr" << endl;
}

void NetCmdStart::execute() {
    cmd_cntrl_->start();
}
