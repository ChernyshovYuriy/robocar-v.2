#ifndef ROBOCAR_NET_CMD_START_H
#define ROBOCAR_NET_CMD_START_H

#include "net_cmd.h"
#include "../cntrl/cmd_cntrl.h"

class NetCmdStart : public NetCmd {

public:
    explicit NetCmdStart(CmdCntrl *cmd_cntrl);

    ~NetCmdStart() override ;

    void execute() override ;

private:
    CmdCntrl *cmd_cntrl_;
};

#endif //ROBOCAR_NET_CMD_START_H
