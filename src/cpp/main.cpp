#include <iostream>
#include "model/cntrl/cntrl_facade.h"

using namespace std;

int main() {
    cout << "Welcome to Robocar!" << endl;

    CntrlFacade facade;
    facade.init();

    cout << "Good bye to Robocar!" << endl;
    return 0;
}
